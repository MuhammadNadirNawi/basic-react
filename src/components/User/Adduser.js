import React from "react";
import classes from "./AddUser.module.css";

const jumlah = () => {
  console.log("function yang bukan compenent function");
  return 1 + 1;
};

const AddUser = (props) => {
  const addUserHander = (event) => {
    console.log(jumlah());
    event.preventDefault();
  };

  return (
    <form onSubmit={addUserHander}>
      <h1>test = {jumlah()}</h1>
      <label htmlFor="username">Username</label>
      <input id="username" type="text"></input>
      <label htmlFor="age">Age (year)</label>
      <input id="age" type="number"></input>
      <button type="submit">Add User</button>
    </form>
  );
};

export default AddUser;
